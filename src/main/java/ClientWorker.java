import message.InMessage;
import message.OutMesssage;

import java.nio.charset.Charset;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class ClientWorker implements Runnable {
    private ConcurrentLinkedQueue<InMessage> inbound;

  public ConcurrentLinkedQueue<InMessage> getInbound() {
    return inbound;
  }

  public ConcurrentLinkedQueue<OutMesssage> getOutbound() {
    return outbound;
  }

  private ConcurrentLinkedQueue<OutMesssage> outbound;
    private final String workerName;


    final String STOP_WORKER_MESSGAE = "STOP_WORKER";
    private boolean workerOn = true;


    ClientWorker(ConcurrentLinkedQueue<InMessage> inbound, ConcurrentLinkedQueue<OutMesssage> outbound, String workerName) {
      this.inbound = inbound;
      this.outbound = outbound;
      this.workerName = workerName;
    }

    @Override
    public void run() {
      while (workerOn){
        try {
          System.out.println("Worker " + workerName + " run!");
          InMessage inMessage = inbound.poll();

          if(inMessage != null ) {
            Thread t = Thread.currentThread();
            String name = t.getName();
            System.out.println("Thread name: "+ name +" inMessage for " + workerName + ": " + inMessage.getBody().toString());
          }

          Thread.sleep((long)(Math.random() * 1000));
          OutMesssage outMesssage = new OutMesssage(generateRandomString(), workerName);
          outbound.add(outMesssage);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }

    String generateRandomString() {
        byte[] array = new byte[7]; // length is bounded by 7
        new Random().nextBytes(array);
        String generatedString = new String(array, Charset.forName("UTF-8"));
        return generatedString;
    }
}
