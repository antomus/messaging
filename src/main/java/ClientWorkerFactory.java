import message.InMessage;
import message.OutMesssage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class ClientWorkerFactory {
    private HashMap<String, ClientWorker> workers = new HashMap<>();
  ConcurrentLinkedQueue<InMessage> inbound;
  ConcurrentLinkedQueue<OutMesssage> outbound;

    ClientWorkerFactory(String [] workerNames){

      this.inbound = inbound;
      this.outbound = outbound;

      for (String workerName : workerNames) {
        workers.put(workerName, createWorkerWithQueues(workerName));
      }
    }

    private ClientWorker createWorkerWithQueues(String workerName) {
      ConcurrentLinkedQueue<InMessage> inbound = new ConcurrentLinkedQueue<InMessage>();
      ConcurrentLinkedQueue<OutMesssage> outbound = new ConcurrentLinkedQueue<OutMesssage>();
      ClientWorker worker = new ClientWorker(inbound, outbound, workerName);

      return worker;
    }

    public HashMap<String, ClientWorker> getWorkers(){
      return workers;
    }
}
