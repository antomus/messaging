import message.Action;
import message.InMessage;
import message.OutMesssage;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Main {
    public static HashMap<String, ClientWorker> workers;
    public static String [] workerNames = {"Server-61850-1","Server-61850-2"};

    public static void main(String [] args) {


      ClientWorkerFactory manager = new ClientWorkerFactory(workerNames);
      workers = manager.getWorkers();


      System.out.println("Start!");

      for (ClientWorker worker : workers.values()) {
        new Thread(worker).start();
      }

      while(true) {
        try {

          processMessagesFromWorkers();

          Thread.sleep((long)(Math.random() * 1000));

          processMessagesToWorkers();

        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }


    }

  static String generateRandomString() {
    byte[] array = new byte[7]; // length is bounded by 7
    new Random().nextBytes(array);
    String generatedString = new String(array, Charset.forName("UTF-8"));
    return generatedString;
  }


  static void processMessagesFromWorkers() {
    for (ClientWorker worker : workers.values()) {
      OutMesssage outMesssage = worker.getOutbound().poll();
      if(outMesssage != null) {
        System.out.println("Thread main outMesssage from " + outMesssage.getClientName() + ": " + outMesssage);
      }

    }
  }

  static void processMessagesToWorkers() {
    for (Map.Entry<String, ClientWorker> entry : workers.entrySet()) {
      ClientWorker worker = entry.getValue();
      String workerName = entry.getKey();
      Random r = new Random();
      int arrIndex = r.nextBoolean() ? 1 : 0;
      Action action = ( arrIndex > 0 ) ? Action.Write : Action.Read;

      InMessage inMessage = new InMessage(generateRandomString(), workerName, action);

      worker.getInbound().add(inMessage);


    }
  }
}
