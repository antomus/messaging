package message;

public class InMessage extends BaseMessage{
  private Action action;

  public InMessage(String body, String clientName, Action action) {
    super(body, clientName, Direction.In);
    this.action = action;
  }
}
