package message;

public class BaseMessage {
    private String body;
    private String clientName;


  public Direction getDirection() {
    return direction;
  }

  public void setDirection(Direction direction) {
    this.direction = direction;
  }

  private Direction direction;

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }


    public BaseMessage(String body, String clientName, Direction direction) {
        this.body = body;
        this.clientName = clientName;
        this.direction = direction;
    }

}
